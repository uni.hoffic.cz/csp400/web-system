<?php
declare(strict_types=1);


namespace App\Twig;


use Doctrine\DBAL\Connection;
use Doctrine\ORM\Query;
use Sonata\AdminBundle\Datagrid\Datagrid;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\RuntimeExtensionInterface;

class QueryManipulator implements RuntimeExtensionInterface
{
    /** @var RequestStack */
    private $requestStack;

    /** @var array */
    private $queryLookup = [];

    /** @var array */
    private $params;

    /** @var Connection */
    private $conn;

    /**
     * QueryManipulator constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function fetchData(Datagrid $datagrid)
    {
        $route = $this->requestStack->getCurrentRequest()->get('_route');

        $query = $datagrid->getQuery()->getQuery();
        $this->queryLookup['${where}'] = $this->extractWhereClause($query);
        $this->queryLookup['${from}'] = 'nhtsa_complaint as n0_';

        $this->params = $query->getParameters()->toArray();
        $this->conn = $query->getEntityManager()->getConnection();

        return [
            'route' => $route,
            'data' => $this->complaintsQueries(),
        ];
    }

    private function complaintsQueries()
    {
        return [
            'total_entries' => $this->x('select count(*) from ${from} where ${where}'),
            'total_injuries' => $this->x('select sum(injured) from ${from} where ${where}'),
            'total_fatalities' => $this->x('select sum(deaths) from ${from} where ${where}'),
            'most_complained_model' => $this->x('select modeltxt from ${from} where ${where} group by modeltxt order by count(*) desc limit 1'),
            'latest_entry' => $this->x('select ldate from ${from} where ${where} order by cmplid desc limit 1'),
            'complaints_series' => $this->x('select yeartxt, count(*) from ${from} where ${where} group by yeartxt order by yeartxt', false),
            'injuries_series' => $this->x('select yeartxt, sum(injured) from ${from} where ${where} group by yeartxt order by yeartxt', false),
            'fatalities_series' => $this->x('select yeartxt, sum(deaths) from ${from} where ${where} group by yeartxt order by yeartxt', false),
            'complaint_components' => $this->x('select split_part(compdesc, \':\', 1) as comp_cat, count(*) from ${from} where ${where} group by comp_cat order by count(*) desc limit 16', false),
            'mileages_at_complaint' => $this->x(<<<'SQL'
select r.range as mileage_range, count(*)
from (
         select '0-9' as range, 9 as endrange
         union all select '10-31', 31
         union all select '31-99', 99
         union all select '100-316', 316
         union all select '317-999', 999
         union all select '1000-3162', 3162
         union all select '3163-9999', 9999
         union all select '10000-31622', 31622
         union all select '31623-99999', 99999
         union all select '100000-316227', 316227
         union all select '316228-MAX', 999999
     ) as r
         left join ${from} on
        r.endrange = case
                         when n0_.miles > 316228 then 999999
                         when n0_.miles > 100000 then 316227
                         when n0_.miles > 31623 then 99999
                         when n0_.miles > 10000 then 31622
                         when n0_.miles > 3162 then 9999
                         when n0_.miles > 1000 then 3162
                         when n0_.miles > 316 then 999
                         when n0_.miles > 100 then 316
                         when n0_.miles > 31 then 99
                         when n0_.miles > 10 then 31
                         when n0_.miles > 0 then 9
        end and ${where}
group by r.range
order by cast(split_part(r.range, '-', 1) as int)

SQL, false),
            'transmission_types' => $this->x('select trans_type, count(*) from ${from} where ${where} group by trans_type order by count(*) desc', false),
            'fuel_systems' => $this->x('select fuel_sys, count(*) from ${from} where ${where} group by fuel_sys order by count(*) desc', false),
            'most_complained_models' => $this->x('select modeltxt, count(*) from ${from} where ${where} group by modeltxt order by count(*) desc limit 8', false),
        ];
    }

    private function x(string $query, $scalar = true)
    {
        $query = str_replace(array_keys($this->queryLookup), $this->queryLookup, $query);
        $st = $this->conn->prepare($query);
        for ($i = 0; $i < count($this->params); $i++) {
            $st->bindValue($i + 1, $this->params[$i]->getValue());
        }
        $st->execute();

        if ($scalar) {
            return $st->fetchOne();
        } else {
            return $st->fetchAllAssociative();
        }
    }

    private function extractWhereClause(Query $query)
    {
        $s1 = explode(' WHERE ', $query->getSQL());

        if (count($s1) === 1) {
            return '0 = 0';

        } else {
            return explode(' LIMIT ', $s1[1])[0];
        }
    }
}
