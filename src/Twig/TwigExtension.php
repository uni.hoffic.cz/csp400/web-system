<?php
declare(strict_types=1);


namespace App\Twig;


use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('fetchData', [QueryManipulator::class, 'fetchData']),
        ];
    }
}
