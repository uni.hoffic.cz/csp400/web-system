<?php
declare(strict_types=1);


namespace App\Repository;


use App\Entity\NhtsaComplaint;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class NhtsaComplaintRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NhtsaComplaint::class);
    }

    public function findChoicesForField(string $field = 'manufacturerName'): array
    {
        $qb = $this->createQueryBuilder('c');
        $qb->select('c.' . $field);
        $qb->distinct();
        $result = $qb->getQuery()->getResult();

        $values = array_column($result, $field);

        return array_combine($values, $values);
    }
}
