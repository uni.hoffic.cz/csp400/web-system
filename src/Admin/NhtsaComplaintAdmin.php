<?php
declare(strict_types=1);


namespace App\Admin;


use App\Repository\NhtsaComplaintRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Filter\ChoiceFilter;
use Sonata\DoctrineORMAdminBundle\Filter\NumberFilter;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class NhtsaComplaintAdmin extends AbstractAdmin
{
    /** @var NhtsaComplaintRepository */
    private $nhtsaComplaintRepository;

    /**
     * NhtsaComplaintAdmin constructor.
     *
     * @param $code
     * @param $class
     * @param null $baseControllerName
     * @param NhtsaComplaintRepository $nhtsaComplaintRepository
     */
    public function __construct(
        $code,
        $class,
        $baseControllerName,
        NhtsaComplaintRepository $nhtsaComplaintRepository
    )
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->nhtsaComplaintRepository = $nhtsaComplaintRepository;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept('list');
    }

    protected function configureListFields(ListMapper $list)
    {
        unset($this->listModes['mosaic']);

        $list->add('i_cmplid');
        $list->add('i_odino');
        $list->add('manufacturerName');
        $list->add('make');
        $list->add('model');
        $list->add('year');
        $list->add('crash');
        $list->add('dateOfIncident');
        $list->add('fire');
        $list->add('injured');
        $list->add('deaths');
        $list->add('componentDescription');
        $list->add('city');
        $list->add('state');
        $list->add('vin');
        $list->add('dateAdded');
        $list->add('dateReceived');
        $list->add('miles');
        $list->add('occurrences');
        $list->add('complaintDescription');
        $list->add('sourceOfComplaint');
        $list->add('reportedToPolice');
        $list->add('purchaseDate');
        $list->add('originalOwner');
        $list->add('antiLockBrakes');
        $list->add('cruiseControl');
        $list->add('numberOfCylinders');
        $list->add('driveTrain');
        $list->add('fuelSystem');
        $list->add('fuelType');
        $list->add('transmissionType');
        $list->add('vehicleSpeed');
        $list->add('i_dot');
        $list->add('tireSize');
        $list->add('locationOfTire');
        $list->add('tireFailureType');
        $list->add('partWasOriginal');
        $list->add('manufactureDate');
        $list->add('childSeatCode');
        $list->add('restraintType');
        $list->add('dealerName');
        $list->add('dealerPhone');
        $list->add('dealerCity');
        $list->add('dealerState');
        $list->add('dealerZip');
        $list->add('productType');
        $list->add('tireRepaired');
        $list->add('medicalAttentionRequired');
        $list->add('vehicleTowed');
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('manufacturerName', ChoiceFilter::class, [], ChoiceType::class, [
            'choices' => $this->nhtsaComplaintRepository->findChoicesForField('manufacturerName'),
        ]);
        $filter->add('make', ChoiceFilter::class, [], ChoiceType::class, [
            'choices' => $this->nhtsaComplaintRepository->findChoicesForField('make'),
        ]);
        $filter->add('model', ChoiceFilter::class, [], ChoiceType::class, [
            'choices' => $this->nhtsaComplaintRepository->findChoicesForField('model'),
        ]);
        $filter->add('year', NumberFilter::class, []);
    }
}
