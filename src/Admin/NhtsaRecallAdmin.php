<?php
declare(strict_types=1);


namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class NhtsaRecallAdmin extends AbstractAdmin
{

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept('list');
    }

    protected function configureListFields(ListMapper $list)
    {
        unset($this->listModes['mosaic']);

        $list->add('i_cmplid');
        $list->add('record_id');
        $list->add('campno');
        $list->add('maketxt');
        $list->add('modeltxt');
        $list->add('yeartxt');
        $list->add('mfgcampno');
        $list->add('compname');
        $list->add('mfgname');
        $list->add('bgman');
        $list->add('endman');
        $list->add('rcltypecd');
        $list->add('potaff');
        $list->add('odate');
        $list->add('influenced_by');
        $list->add('mfgtxt');
        $list->add('rcdate');
        $list->add('datea');
        $list->add('rpno');
        $list->add('fmvss');
        $list->add('desc_defect');
        $list->add('conequence_defect');
        $list->add('corrective_action');
        $list->add('notes');
        $list->add('rcl_cmpt_id');
        $list->add('mfr_comp_name');
        $list->add('mfr_comp_ptno');
    }
}
