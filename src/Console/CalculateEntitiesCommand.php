<?php
declare(strict_types=1);


namespace App\Console;


use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class CalculateEntitiesCommand extends Command
{
    /** @var Connection */
    private $connection;

    /** @var KernelInterface */
    private $kernel;

    /**
     * CalculateEntitiesCommand constructor.
     * @param Connection $connection
     * @param KernelInterface $kernel
     */
    public function __construct(Connection $connection, KernelInterface $kernel)
    {
        parent::__construct('wp:entities:calculate');

        $this->connection = $connection;
        $this->kernel = $kernel;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Executing query...');
        $this->connection->exec(file_get_contents(
            $this->kernel->getProjectDir() . '/assets/queries/calculate_entities.sql'
        ));
        $output->writeln('Done!');
    }
}
