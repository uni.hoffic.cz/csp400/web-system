<?php
declare(strict_types=1);


namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Make
 *
 * @package App\Entity
 * @ORM\Entity
 */
class Make
{
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    public $name;

    /**
     * @var Collection|NhtsaComplaint[]
     * @ORM\OneToMany(targetEntity="NhtsaComplaint", mappedBy="makeEnt")
     */
    public $complaints;

    /**
     * @var Collection|NhtsaRecall[]
     * @ORM\OneToMany(targetEntity="NhtsaRecall", mappedBy="makeEnt")
     */
    public $recalls;

    public function __toString()
    {
        return $this->name;
    }
}
