<?php
declare(strict_types=1);


namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class NhtsaComplaint
 *
 * @package App\Entity
 * @ORM\Entity
 */
class NhtsaComplaint
{
    /**
     * @var Manufacturer|null
     * @ORM\ManyToOne(targetEntity="Manufacturer")
     */
    public $manufacturer;

    /**
     * @var Make|null
     * @ORM\ManyToOne(targetEntity="Make")
     */
    public $makeEnt;

    /**
     * @var Model|null
     * @ORM\ManyToOne(targetEntity="Model")
     */
    public $modelEnt;

    public function __toString()
    {
        return sprintf('%s', $this->i_cmplid);
    }

    // DEFAULT FIELDS BELOW

    /**
     * @var integer|null
     * @ORM\Column(name="CMPLID", type="integer", nullable=true)
     * @ORM\Id
     */
    public $i_cmplid;

    /**
     * @var integer|null
     * @ORM\Column(name="ODINO", type="integer", nullable=true)
     */
    public $i_odino;

    /**
     * @var string|null
     * @ORM\Column(name="MFR_NAME", type="string", length=40, nullable=true)
     */
    public $manufacturerName;

    /**
     * @var string|null
     * @ORM\Column(name="MAKETXT", type="string", length=25, nullable=true)
     */
    public $make;

    /**
     * @var string|null
     * @ORM\Column(name="MODELTXT", type="string", length=256, nullable=true)
     */
    public $model;

    /**
     * @var integer|null
     * @ORM\Column(name="YEARTXT", type="integer", nullable=true)
     */
    public $year;

    /**
     * @var boolean|null
     * @ORM\Column(name="CRASH", type="boolean", nullable=true)
     */
    public $crash;

    /**
     * @var DateTime|null
     * @ORM\Column(name="FAILDATE", type="date", nullable=true)
     */
    public $dateOfIncident;

    /**
     * @var boolean|null
     * @ORM\Column(name="FIRE", type="boolean", nullable=true)
     */
    public $fire;

    /**
     * @var int|null
     * @ORM\Column(name="INJURED", type="integer", nullable=true)
     */
    public $injured;

    /**
     * @var int|null
     * @ORM\Column(name="DEATHS", type="integer", nullable=true)
     */
    public $deaths;

    /**
     * @var string|null
     * @ORM\Column(name="COMPDESC", type="string", length=128, nullable=true)
     */
    public $componentDescription;

    /**
     * @var string|null
     * @ORM\Column(name="CITY", type="string", length=30, nullable=true)
     */
    public $city;

    /**
     * @var string|null
     * @ORM\Column(name="STATE", type="string", length=2, nullable=true)
     */
    public $state;

    /**
     * @var string|null
     * @ORM\Column(name="VIN", type="string", length=11, nullable=true)
     */
    public $vin;

    /**
     * @var DateTime|null
     * @ORM\Column(name="DATEA", type="date", nullable=true)
     */
    public $dateAdded;

    /**
     * @var DateTime|null
     * @ORM\Column(name="LDATE", type="date", nullable=true)
     */
    public $dateReceived;

    /**
     * @var integer|null
     * @ORM\Column(name="MILES", type="integer", nullable=true)
     */
    public $miles;

    /**
     * @var int|null
     * @ORM\Column(name="OCCURENCES", type="integer", nullable=true)
     */
    public $occurrences;

    /**
     * @var string|null
     * @ORM\Column(name="CDESCR", type="text", nullable=true)
     */
    public $complaintDescription;

    /**
     * @var string|null
     * @ORM\Column(name="CMPL_TYPE", type="string", length=4, nullable=true)
     */
    public $sourceOfComplaint;

    /**
     * @var boolean|null
     * @ORM\Column(name="POLICE_RPT_YN", type="boolean", nullable=true)
     */
    public $reportedToPolice;

    /**
     * @var DateTime|null
     * @ORM\Column(name="PURCH_DT", type="date", nullable=true)
     */
    public $purchaseDate;

    /**
     * @var boolean|null
     * @ORM\Column(name="ORIG_OWNER_YN", type="boolean", nullable=true)
     */
    public $originalOwner;

    /**
     * @var boolean|null
     * @ORM\Column(name="ANTI_BRAKES_YN", type="boolean", nullable=true)
     */
    public $antiLockBrakes;

    /**
     * @var boolean|null
     * @ORM\Column(name="CRUISE_CONT_YN", type="boolean", nullable=true)
     */
    public $cruiseControl;

    /**
     * @var int|null
     * @ORM\Column(name="NUM_CYLS", type="integer", nullable=true)
     */
    public $numberOfCylinders;

    /**
     * @var string|null
     * @ORM\Column(name="DIVE_TRAIN", type="string", length=4, nullable=true)
     */
    public $driveTrain;

    /**
     * @var string|null
     * @ORM\Column(name="FUEL_SYS", type="string", length=4, nullable=true)
     */
    public $fuelSystem;

    /**
     * @var string|null
     * @ORM\Column(name="FUEL_TYPE", type="string", length=4, nullable=true)
     */
    public $fuelType;

    /**
     * @var string|null
     * @ORM\Column(name="TRANS_TYPE", type="string", length=4, nullable=true)
     */
    public $transmissionType;

    /**
     * @var int|null
     * @ORM\Column(name="VEH_SPEED", type="integer", nullable=true)
     */
    public $vehicleSpeed;

    /**
     * @var string|null
     * @ORM\Column(name="DOT", type="string", length=20, nullable=true)
     */
    public $i_dot;

    /**
     * @var string|null
     * @ORM\Column(name="TIRE_SIZE", type="string", length=30, nullable=true)
     */
    public $tireSize;

    /**
     * @var string|null
     * @ORM\Column(name="LOC_OF_TIRE", type="string", length=4, nullable=true)
     */
    public $locationOfTire;

    /**
     * @var string|null
     * @ORM\Column(name="TIRE_FAIL_TYPE", type="string", length=4, nullable=true)
     */
    public $tireFailureType;

    /**
     * @var boolean|null
     * @ORM\Column(name="ORIG_EQUIP_YN", type="boolean", nullable=true)
     */
    public $partWasOriginal;

    /**
     * @var DateTime|null
     * @ORM\Column(name="MANUF_DT", type="date", nullable=true)
     */
    public $manufactureDate;

    /**
     * @var string|null
     * @ORM\Column(name="SEAT_TYPE", type="string", length=4, nullable=true)
     */
    public $childSeatCode;

    /**
     * @var string|null
     * @ORM\Column(name="RESTRAINT_TYPE", type="string", length=4, nullable=true)
     */
    public $restraintType;

    /**
     * @var string|null
     * @ORM\Column(name="DEALER_NAME", type="string", length=40, nullable=true)
     */
    public $dealerName;

    /**
     * @var string|null
     * @ORM\Column(name="DEALER_TEL", type="string", length=20, nullable=true)
     */
    public $dealerPhone;

    /**
     * @var string|null
     * @ORM\Column(name="DEALER_CITY", type="string", length=30, nullable=true)
     */
    public $dealerCity;

    /**
     * @var string|null
     * @ORM\Column(name="DEALER_STATE", type="string", length=2, nullable=true)
     */
    public $dealerState;

    /**
     * @var string|null
     * @ORM\Column(name="DEALER_ZIP", type="string", length=10, nullable=true)
     */
    public $dealerZip;

    /**
     * @var string|null
     * @ORM\Column(name="PROD_TYPE", type="string", length=4, nullable=true)
     */
    public $productType;

    /**
     * @var boolean|null
     * @ORM\Column(name="REPAIRED_YN", type="boolean", nullable=true)
     */
    public $tireRepaired;

    /**
     * @var boolean|null
     * @ORM\Column(name="MEDICAL_ATTN", type="boolean", nullable=true)
     */
    public $medicalAttentionRequired;

    /**
     * @var boolean|null
     * @ORM\Column(name="VEHICLE_TOWED_YN", type="boolean", nullable=true)
     */
    public $vehicleTowed;
}
