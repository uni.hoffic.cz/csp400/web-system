<?php
declare(strict_types=1);


namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Manufacturer
 *
 * @package App\Entity
 * @ORM\Entity
 */
class Manufacturer
{
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    public $name;

    /**
     * @var Collection|NhtsaComplaint[]
     * @ORM\OneToMany(targetEntity="NhtsaComplaint", mappedBy="manufacturer")
     */
    public $complaints;

    /**
     * @var Collection|NhtsaRecall[]
     * @ORM\OneToMany(targetEntity="NhtsaRecall", mappedBy="manufacturer")
     */
    public $recalls;
}
