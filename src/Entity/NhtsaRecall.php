<?php
declare(strict_types=1);


namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class NhtsaRecall
 *
 * @package App\Entity
 * @ORM\Entity
 */
class NhtsaRecall
{
    /**
     * @var Manufacturer|null
     * @ORM\ManyToOne(targetEntity="Manufacturer")
     */
    public $manufacturer;

    /**
     * @var Model|null
     * @ORM\ManyToOne(targetEntity="Model")
     */
    public $modelEnt;

    /**
     * @var Make|null
     * @ORM\ManyToOne(targetEntity="Make")
     */
    public $makeEnt;

    public function __toString()
    {
        return sprintf('%s', $this->record_id);
    }

    // DEFAULT FIELDS BELOW

    /**
     * @var int|null
     * @ORM\Column(name="RECORD_ID", type="integer", nullable=true)
     * @ORM\Id
     */
    public $record_id;


    /**
     * @var string|null
     * @ORM\Column(name="CAMPNO", type="string", nullable=true)
     */
    public $campno;


    /**
     * @var string|null
     * @ORM\Column(name="MAKETXT", type="string", nullable=true)
     */
    public $maketxt;


    /**
     * @var string|null
     * @ORM\Column(name="MODELTXT", type="string", nullable=true)
     */
    public $modeltxt;


    /**
     * @var int|null
     * @ORM\Column(name="YEARTXT", type="integer", nullable=true)
     */
    public $yeartxt;


    /**
     * @var string|null
     * @ORM\Column(name="MFGCAMPNO", type="string", nullable=true)
     */
    public $mfgcampno;


    /**
     * @var string|null
     * @ORM\Column(name="COMPNAME", type="string", nullable=true)
     */
    public $compname;


    /**
     * @var string|null
     * @ORM\Column(name="MFGNAME", type="string", nullable=true)
     */
    public $mfgname;


    /**
     * @var DateTime|null
     * @ORM\Column(name="BGMAN", type="date", nullable=true)
     */
    public $bgman;


    /**
     * @var DateTime|null
     * @ORM\Column(name="ENDMAN", type="date", nullable=true)
     */
    public $endman;


    /**
     * @var string|null
     * @ORM\Column(name="RCLTYPECD", type="string", nullable=true)
     */
    public $rcltypecd;


    /**
     * @var integer|null
     * @ORM\Column(name="POTAFF", type="integer", nullable=true)
     */
    public $POTAFF;


    /**
     * @var DateTime|null
     * @ORM\Column(name="ODATE", type="date", nullable=true)
     */
    public $odate;


    /**
     * @var string|null
     * @ORM\Column(name="INFLUENCED_BY", type="string", nullable=true)
     */
    public $influenced_by;


    /**
     * @var string|null
     * @ORM\Column(name="MFGTXT", type="string", nullable=true)
     */
    public $mfgtxt;


    /**
     * @var DateTime|null
     * @ORM\Column(name="RCDATE", type="date", nullable=true)
     */
    public $rcdate;


    /**
     * @var DateTime|null
     * @ORM\Column(name="DATEA", type="date", nullable=true)
     */
    public $datea;


    /**
     * @var int|null
     * @ORM\Column(name="RPNO", type="integer", nullable=true)
     */
    public $rpno;


    /**
     * @var string|null
     * @ORM\Column(name="FMVSS", type="string", nullable=true)
     */
    public $fmvss;


    /**
     * @var string|null
     * @ORM\Column(name="DESC_DEFECT", type="text", nullable=true)
     */
    public $desc_defect;


    /**
     * @var string|null
     * @ORM\Column(name="CONEQUENCE_DEFECT", type="text", nullable=true)
     */
    public $conequence_defect;


    /**
     * @var string|null
     * @ORM\Column(name="CORRECTIVE_ACTION", type="text", nullable=true)
     */
    public $corrective_action;


    /**
     * @var string|null
     * @ORM\Column(name="NOTES", type="text", nullable=true)
     */
    public $notes;


    /**
     * @var string|null
     * @ORM\Column(name="RCL_CMPT_ID", type="string", nullable=true)
     */
    public $rcl_cmpt_id;


    /**
     * @var string|null
     * @ORM\Column(name="MFR_COMP_NAME", type="string", nullable=true)
     */
    public $mfr_comp_name;


    /**
     * @var string|null
     * @ORM\Column(name="MFR_COMP_DESC", type="string", nullable=true)
     */
    public $mfr_comp_desc;


    /**
     * @var string|null
     * @ORM\Column(name="MFR_COMP_PTNO", type="string", nullable=true)
     */
    public $mfr_comp_ptno;
}
