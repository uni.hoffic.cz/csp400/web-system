set -e

echo "Downloading complaints data..."
wget https://www-odi.nhtsa.dot.gov/downloads/folders/Complaints/FLAT_CMPL.zip -O /tmp/FLAT_CMPL.zip

echo "Downloading investigations data..."
wget https://www-odi.nhtsa.dot.gov/downloads/folders/Investigations/FLAT_INV.zip -O /tmp/FLAT_INV.zip

echo "Downloading recalls data..."
wget https://www-odi.nhtsa.dot.gov/downloads/folders/Recalls/FLAT_RCL.zip -O /tmp/FLAT_RCL.zip

echo "Downloading technicals data..."
wget https://www-odi.nhtsa.dot.gov/downloads/folders/TSBS/FLAT_TSBS.zip -O /tmp/FLAT_TSBS.zip

echo "Unpacking complaints data..."
unzip -o /tmp/FLAT_CMPL.zip -d /tmp/

echo "Unpacking investigations data..."
unzip -o /tmp/FLAT_INV.zip -d /tmp/

echo "Unpacking recalls data..."
unzip -o /tmp/FLAT_RCL.zip -d /tmp/

echo "Unpacking technicals data..."
unzip -o /tmp/FLAT_TSBS.zip -d /tmp/

echo "Importing complaints data..."
cd /var/www/html/modules/nhtsa_parser/
./gradlew run

echo "Calculating entities..."
cd /var/www/html/
php bin/console wp:entities:calculate
