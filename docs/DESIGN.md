# Design

The application is designed to be self-contained so it can be easily deployed
on the user's machine or a server for the whole team to use it simultaneously.
For this reason, all dependencies, environmental requirements and networking
are defined in code. This is achieved using [Docker](https://www.docker.com/)
and related tools.

![](http://116.203.37.141:8080/png/VL7BJiCm43p3Lwp21_0DWIALga6XGN1073REDhNYsDRQ1aMe_uxJncaHZuDjUNPcrZksA3fi2RA54Hqnt9hU6aUGC-EGmPMIxSxNAX1yNW9i6Vl8pQrLKg2NHaU1zYWrSIAtJAGFx9bVBilhQkYtHW_mGZL4VMH_xAIdYGZmUXDSj74ErgQLuYt1TzfBFq21rahTpl02FS9zV2jxg0HBwrDfEgRM6qBXLnKKfWcAObUhGZytZKRFqWLKqXscSSba_wlP2j_nRwU7vLELGudiYB_xZFkuJWcEpwynvlnFV1TD8cMbSMEXUbn3KluCyDX__40L7VqyorJOa42bJcY1EcnZE869pz6EpcwceIGv3h7_0W00)

There are 2 containers inside the Docker Compose array, and they are connected by
an internal network. The web application and all other tools use this network to
communicate with the database without exposing the database outside localhost.

We chose [PostgreSQL](https://www.postgresql.org/) as our database since it is
relational, which well describes the type of our data, and can handle large amounts
of automotive industry data. There were other options possibly with higher performance
from Oracle and other vendors, but those have unsuitable licensing terms and
come with a proprietary flavour of the SQL language, which we deemed was not
worth learning at this stage. The system has been designed to allow simple
migration to a different SQL-compatible database down the line, so alternatives
can be considered in the future.

We chose [Symfony](https://symfony.com/) as our web framework, because some
team members were already familiar with it and it has powerful ORM abstraction,
which would help with relating our data. Furthermore, it is very flexible and
allowed us to embed modules coded in other languages within and integrate them
with the web application's business logic.

The first embedded module is a parser for the NHTSA data set written in Java.
The NHTSA data is structured and available in the CSV format, however, it does
not adhere to its definition and many rows are corrupted. This meant we had
to parse this data ourselves rather than relying on the built-in importers that
come with the database. We also tried parsing the data set in PHP since we
were already using it, but the performance was prohibitively low as this is not
the intended use case. We chose Java for its performance and familiarity and
used [Gradle](https://gradle.org/) for dependency management. The only dependency
ended up being the database client SDK.

The second embedded module is a [ReactJS](https://reactjs.org/) app, to which
the aggregated statistical data is being passed using HTML data attributes and
which renders the visualisations. This library works well with others such as
[ChartJS](https://www.chartjs.org/) and results in a minimal, clean and readable
code unlike using plain JavaScript. At the time of pursuing this project there
is no alternative technology to be considered as the team members are not familiar
with them and this decision is trivial to change later.

## Database Schema
The business desires information related to manufacturers, car makes and models.
In order to provide this information, the data needs to be in a format that allows
indexing by these entities. See the diagram below.

![](http://116.203.37.141:8080/png/VOz13eCm30JlUGLIpxm9kS50X75GS0weFBu24iafbEkEPsqZYvErce1Z1rPYm52zoFM3l2cYmOn7l8jzYwJp3BguDpKj22JS1qGsvYZQjhhsdzFU8xKw8HvaWSRwQIPmB0yp0IqG5RrN-PTbwJZ_dtM5IRkzHdrSB2noNQd05m00)

We can generate statistical and analytical insight from the complaints and recalls
tables individually and also relate these tables by their manufacturer, model, make
or other fields such as year or component not displayed on the diagram. This also
allows the introduction of other data sets in the future that may not otherwise
be compatible.

The nhtsa_complaints and nhtsa_recalls tables contain 51 and 24 columns respectively.
All are mapped to their corresponding objects.

## Symfony Web App
The Symfony framework comes with Doctrine ORM and the Sonata Admin content management
system. The database tables are mapped using annotations and using Sonata Admin
configurations. These map the individual tables and table columns to dashboard pages
and page columns. These are configured to translate filter options, sorting instructions
and relational operations into SQL and display the result in a formatted web page.
The abstraction layers can be seen in a diagram below.

![](http://116.203.37.141:8080/png/NP31JWCn34Jl-nLMJ-LGSUy12R482L4LMX1d9x5e88r3uW3zUvpTjfJUelYDFJC9itZh4qQ1bq8Pi43jqidlv9KCz5WsXZ4xcAuveY1SkwsF5vF6k__y0bCY1qlpJbPWshELx2F1ShKOUVxFzxhEO26OFdAHjqphfuVzOjDr5MQpAt1pk0dUVW16e41RYjAvOS9pUCuOIq0XG2kU8mW3ZzTJvaHPTfVZfEDncpaLdJ5D_LafxpfdZlJj3zcgx31G4muoISqARqYVfGOvu6gefvbMwJ7uGdjG2ydyNS37exboS3E75KMdtRTbxjXqkz9-Qd9gqu7bA9b3sGUlUiUN571tfjC3Wqmj_A5t52ctj5--r6OCMzRcPu1NfE7lrx1bLmFz0W00)

Because the user interacts with the database using mappings and all the
statistical and analytical processing is done on the database, we achieve the best
performance as we are operating on the minimal data required for any calculation
and the calculations are optimised better than any PHP or Java-based approach we
could realistically implement.

## Data Importing
The data importing pipeline is automated so that at any point in time the user
can choose to fetch the newest data. The data is first downloaded and unzipped
using the standard linux utilities. Then our custom NHTSA parser written in Java
parses and imports the NHTSA data set and finally, an SQL script constructs
the relational mapping for the imported entities.

![](http://116.203.37.141:8080/png/ZLBBRjj03BphAmZd8X_o0mdGeaqJj4KQf7OkHT43BN4b0gkbi4l5DOhyUwc7RSKrYbm4InuEvy6a63LhxS7A4c3n4LCDwo9ogmluam5iQ_O4-LgwM522I0rou52zNm8do7zGMXeAuEgJR8CNB45hWX8LmKLfG9pZWj536XckH_JjRurOw17CGSNOiSIDxNCI6zG1JA6SD3p8ChCFCxKFdv_M7-0HOw88-LTynkK2BZnNjMvyHuE1WuMddZc19sTh1Tee9YUT-Z1jA-fT33eA2OwhBcA_xte2J7mhsRw1RSzNYE-Q12KvYs_EPe17-x_Xb2pM4HSIR5dh7esTLtX6SvDUJPZbaOpACK3TjNH4i8Dy0FMROwH2_Sx4MTm1yjrSdh5zQLg9UXmcR-Hva5UWUwd08VikpjPOEUoom-zPKNl4vSyBYHWg-dNvBZi7UpdfT4L-I3l13VgYinUnRjVVx_zJxJSfJVEXZ68Qf3Fbtd4e0Rs7afDoi1Di-k6JYAzkx1IL8FOTsp42OL6V7JbEx9VQ5UyTRB0lm5prPzv6RZ3k05KZRpgTHl_DuoLBAkrV)

The script could be run while the system is being used, though performance will
suffer. The system recognizes existing data and during an update it will append
to it.

## ReactJS Visualisations

Each dashboard view can display visualisations of the data that is selected
on the dashboard page. The SQL queries for statistics and analytics are generated
from the filters applied in the dashboard view. The queries are written in SQL
with some parts being dynamically replaced at runtime. See an example below:

```sql
select sum(injured) from ${from} where ${where}
```

The `${from}` token may be replaced by `nhtsa_complaint as n0_` while the
`where` token may be replaced by `maketxt = 'FORD' and year > 2010'`, depending
on the view and filters applied.

These queries are executed and the results are being injected as html data
attributes. From there, the embedded ReactJS initializer will read the data
and pass it to the React application as React Props. The ReactJS module is
a standalone JavaScript application which happens to be embedded inside the
dashboard pages and happens to read data passed to it.

The final visualisations are very simple charts generated from the pre-formatted
data being passed in and for our intensive purposes, it does not really matter
which visualisation library we choose. We went with ChartJS because it is easy
to work with and some team members had had experience working with it. It would be
very simple to migrate to an alternative as the visualisation module is an
embedded stand-alone app and does not share dependencies with the rest of the
system.

## Docker Containers
As said in the introduction, the application consists of containers connected
by an internal network. Each container contains a custom runtime supporting services
in that container. This is useful for managing dependencies and for use with
orchestrators such as Kubernetes, which manage the life cycle of the containers
and manage the deployment of the system.

![](http://116.203.37.141:8080/png/XOvB3e90343NS8e63-0PY2HgOiWe2zqMB301PaWPFnF3tHr5D2H65rtrlRwsc5LO40IHpYfYMEYcrHr1o8msW5ajYzAazOdWvWFaZ8rZmxQjPOP6QkK4PL0guX4kc4WzwJSlLcAUs2RNoiAUKd2-eo-bDFG2Ut-OJoD2WocwJtu4lEC81K9tZqMotS1e_at4otLycDpNZAeO16y2xUXCx3g2zTL-FTpV0G00)

The Docker Compose array is currently configured to serve traffic on port `8080`,
but there is an NGINX reverse proxy readily available. It is inactive by default
because the set up requires certificate generation and is unnecessarily complex
for localhost use, though available for server use.

# Code Structure

Below you will find a part of the project's directory tree with descriptions of
contents. Some directories have been omitted.

```text
.                                   # Root directory containing configuration files
├── assets
│   ├── images                      # Static image assets
│   ├── js                          # JavaScript + ReactJs visualisation module
│   ├── queries                     # SQL queries to process the data
│   └── styles                      # CSS styles
├── bin                             # Framework binary files (do not touch)
├── config                          # Framework configuration files
├── docs                            # Project documentation
├── migrations                      # Migrations to set up and update the database
├── modules
│   └── nhtsa_parser                # Java NHTSA data set parser module
├── ops
│   ├── data                        # Scripts to import and manipulate data
│   └── docker                      # Definitions of the runtime environments
├── public                          # Generated files (do not touch)
├── src
│   ├── Admin                       # Configurations of the data presentation dashboard
│   ├── Console                     # Entry points for console commands
│   ├── Controller                  # Entry points for the web pages
│   ├── Entity                      # Objects representing the relational schema
│   ├── Repository                  # Database access abstraction layers
│   └── Twig                        # Logic for supplying data to the HTML data attributes
├── templates                       # Twig HTML templates for rendering pages
├── tests                           # Not in use
└── translations                    # Not in use
```
