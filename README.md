# CSP400 - We Predict

## Setting up for development

#### Prerequisites
- Docker
- Docker Compose
- Linux-like environment (you can probably make it work under windows too)

#### Hello World
1. Clone the project and navigate to its root directory
1. `docker-compose up`
1. Open a new terminal window in the same directory
1. `docker-compose exec app ash`
1. `composer install`
1. `yarn install`
1. `yarn build --dev`
1. Navigate to http://localhost:8080/

#### Import Nhtsa Data
1. `docker-compose exec app ash -c "php bin/console doctrine:database:create"`
1. `docker-compose exec app ash -c "php bin/console doctrine:migrations:migrate --no-interaction"`
1. `docker-compose exec app ash ops/data/import_nhtsa.sh`
