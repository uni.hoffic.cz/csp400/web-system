package com.wepredict.nhtsaparser.definition;

import com.wepredict.nhtsaparser.model.Field;

import java.time.LocalDate;
import java.util.stream.Stream;

public class FieldDefinitions {

    public static Field[] defineComplaints() {
        return Stream.of(
                new Field("cmplid", Integer.class),
                new Field("odino", Integer.class),
                new Field("mfr_name", String.class),
                new Field("maketxt", String.class),
                new Field("modeltxt", String.class),
                new Field("yeartxt", Integer.class),
                new Field("crash", Boolean.class),
                new Field("faildate", LocalDate.class),
                new Field("fire", Boolean.class),
                new Field("injured", Integer.class),
                new Field("deaths", Integer.class),
                new Field("compdesc", String.class),
                new Field("city", String.class),
                new Field("state", String.class),
                new Field("vin", String.class),
                new Field("datea", LocalDate.class),
                new Field("ldate", LocalDate.class),
                new Field("miles", Integer.class),
                new Field("occurences", Integer.class),
                new Field("cdescr", String.class),
                new Field("cmpl_type", String.class),
                new Field("police_rpt_yn", Boolean.class),
                new Field("purch_dt", LocalDate.class),
                new Field("orig_owner_yn", Boolean.class),
                new Field("anti_brakes_yn", Boolean.class),
                new Field("cruise_cont_yn", Boolean.class),
                new Field("num_cyls", Integer.class),
                new Field("dive_train", String.class),
                new Field("fuel_sys", String.class),
                new Field("fuel_type", String.class),
                new Field("trans_type", String.class),
                new Field("veh_speed", Integer.class),
                new Field("dot", String.class),
                new Field("tire_size", String.class),
                new Field("loc_of_tire", String.class),
                new Field("tire_fail_type", String.class),
                new Field("orig_equip_yn", Boolean.class),
                new Field("manuf_dt", LocalDate.class),
                new Field("seat_type", String.class),
                new Field("restraint_type", String.class),
                new Field("dealer_name", String.class),
                new Field("dealer_tel", String.class),
                new Field("dealer_city", String.class),
                new Field("dealer_state", String.class),
                new Field("dealer_zip", String.class),
                new Field("prod_type", String.class),
                new Field("repaired_yn", Boolean.class),
                new Field("medical_attn", Boolean.class),
                new Field("vehicle_towed_yn", Boolean.class)
        ).toArray(Field[]::new);
    }

    public static Field[] defineRecalls() {
        return Stream.of(
                new Field("record_id", Integer.class),
                new Field("campno", String.class),
                new Field("maketxt", String.class),
                new Field("modeltxt", String.class),
                new Field("yeartxt", Integer.class),
                new Field("mfgcampno", String.class),
                new Field("compname", String.class),
                new Field("mfgname", String.class),
                new Field("bgman", LocalDate.class),
                new Field("endman", LocalDate.class),
                new Field("rcltypecd", String.class),
                new Field("potaff", Integer.class),
                new Field("odate", LocalDate.class),
                new Field("influenced_by", String.class),
                new Field("mfgtxt", String.class),
                new Field("rcdate", LocalDate.class),
                new Field("datea", LocalDate.class),
                new Field("rpno", Integer.class),
                new Field("fmvss", Integer.class),
                new Field("desc_defect", String.class),
                new Field("conequence_defect", String.class),
                new Field("corrective_action", String.class),
                new Field("notes", String.class),
                new Field("rcl_cmpt_id", String.class),
                new Field("mfr_comp_name", String.class),
                new Field("mfr_comp_ptno", String.class)
        ).toArray(Field[]::new);
    }
}
