package com.wepredict.nhtsaparser.model;

public class Field {
    private final String name;
    private final Class fieldType;

    public Field(String name, Class fieldType) {
        this.name = name;
        this.fieldType = fieldType;
    }

    public String getName() {
        return name;
    }

    public Class getFieldType() {
        return fieldType;
    }
}
