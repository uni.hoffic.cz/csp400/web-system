package com.wepredict.nhtsaparser;

import com.wepredict.nhtsaparser.definition.FieldDefinitions;
import com.wepredict.nhtsaparser.model.Field;
import com.wepredict.nhtsaparser.model.Table;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    static final String INPUT_DELIMITER = "\t";
    static final String DB_URL = "jdbc:postgresql://db/application";

    public static void main(String[] args) throws Exception {
        Properties props = new Properties();
        props.setProperty("user", "postgres");
        props.setProperty("password", System.getenv("PGPASSWORD"));
        props.setProperty("ssl", "false");
        Connection conn = DriverManager.getConnection(DB_URL, props);
        conn.setAutoCommit(false);

        var tables = Stream.of(
                new Table("nhtsa_complaint", "/tmp/FLAT_CMPL.txt", FieldDefinitions.defineComplaints()),
                new Table("nhtsa_recall", "/tmp/FLAT_RCL.txt", FieldDefinitions.defineRecalls())
        );

        tables.forEach(table -> importTable(conn, table));

        conn.commit();
        conn.close();
    }

    private static void importTable(Connection conn, Table table) {
        try {
            FileReader fr = new FileReader(table.getFile(), StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader(fr);

            int counter = 0;

            String str;
            while ((str = reader.readLine()) != null) {
                insertRow(conn, table, str);
                counter++;

                if (counter % 10000 == 0) {
                    conn.commit();
                    System.out.printf("%s entries processed.%n", counter);
                }
            }
            reader.close();
            fr.close();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void insertRow(Connection conn, Table table, String inputLine) {
        try {
            var tableSize = table.getFields().length;

            var columns = new ArrayList<String>();
            Collections.addAll(columns, inputLine.toUpperCase().split(INPUT_DELIMITER));

            // Padding missing columns
            for (int i = columns.size(); i < tableSize; i++) {
                columns.add("");
            }

            var fieldNames = Arrays.stream(table.getFields())
                    .map(Field::getName)
                    .collect(Collectors.joining(","));
            var statement = conn.prepareStatement("INSERT INTO " + table.getName() + "(" + fieldNames + ") VALUES(?" + ",?".repeat(tableSize - 1) + ") ON CONFLICT DO NOTHING");

            for (int i = 0; i < tableSize; i++) {
                var columnType = table.getFields()[i].getFieldType();

                if (columns.get(i).isEmpty()) {
                    statement.setObject(i + 1, null);

                } else if (columnType.equals(String.class)) {
                    statement.setString(i + 1, columns.get(i));

                } else if (columnType.equals(Integer.class)) {
                    statement.setInt(i + 1, Integer.parseInt(columns.get(i)));

                } else if (columnType.equals(Boolean.class)) {
                    statement.setBoolean(i + 1, columns.get(i).equals("Y"));

                } else if (columnType.equals(LocalDate.class)) {
                    var dateFormat = new SimpleDateFormat("yyyyMMdd");
                    statement.setDate(i + 1, new Date(dateFormat.parse(columns.get(i)).getTime()));
                }
            }

            statement.execute();

        } catch (NumberFormatException | ParseException e) {
            System.err.println("Row corrupted, failed to parse...");

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
