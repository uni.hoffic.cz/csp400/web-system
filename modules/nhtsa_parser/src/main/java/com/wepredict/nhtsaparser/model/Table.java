package com.wepredict.nhtsaparser.model;

public class Table {
    private final String name;
    private final String file;
    private final Field[] fields;

    public Table(String name, String file, Field[] fields) {
        this.name = name;
        this.file = file;
        this.fields = fields;
    }

    public String getName() {
        return name;
    }

    public String getFile() {
        return file;
    }

    public Field[] getFields() {
        return fields;
    }
}
