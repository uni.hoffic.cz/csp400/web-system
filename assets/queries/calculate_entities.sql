-- Manufacturers
with manufacturers as (
    select distinct mfr_name
    from nhtsa_complaint
    where mfr_name is not null
    union
    distinct
    select distinct mfgname
    from nhtsa_recall
    where mfgname is not null
)
insert
into manufacturer(name)
select *
from manufacturers;

update nhtsa_complaint
set manufacturer_id = manufacturer.id
from manufacturer
where nhtsa_complaint.mfr_name = manufacturer.name;

update nhtsa_recall
set manufacturer_id = manufacturer.id
from manufacturer
where nhtsa_recall.mfgname = manufacturer.name;

-- Makes
with makes as (
    select distinct maketxt
    from nhtsa_complaint
    where maketxt is not null
    union
    distinct
    select distinct maketxt
    from nhtsa_recall
    where maketxt is not null
)
insert
into make(name)
select *
from makes;

update nhtsa_complaint
set make_ent_id = make.id
from make
where nhtsa_complaint.maketxt = make.name;

update nhtsa_recall
set make_ent_id = make.id
from make
where nhtsa_recall.maketxt = make.name;

-- Models
with models as (
    select distinct make_ent_id, modeltxt
    from nhtsa_complaint
    where make_ent_id is not null
      and modeltxt is not null
    union
    distinct
    select distinct make_ent_id, modeltxt
    from nhtsa_recall
    where make_ent_id is not null
      and modeltxt is not null
)
insert
into model(make_id, name)
select *
from models;

update nhtsa_complaint
set model_ent_id = model.id
from model
where nhtsa_complaint.make_ent_id = model.make_id
  and nhtsa_complaint.modeltxt = model.name;

update nhtsa_recall
set model_ent_id = model.id
from model
where nhtsa_recall.make_ent_id = model.make_id
  and nhtsa_recall.modeltxt = model.name;
