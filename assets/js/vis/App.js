import React from 'react';
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Cards from './Components/Cards.js';
import Charts from './Components/Charts.js';
import theme from './theme';
import Collapsible from 'react-collapsible';

const useStyles = makeStyles((theme) => ({

  body: {
    backgroundColor: '#e0e0e0',
  },
  container: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    background: 'linear-gradient(90deg, #1F436D 30%, #B51383 90%)',
    color: '#fcfcfc',
  },
}));

export const App = (props) => {
  const classes = useStyles();

  console.log('Route', props.route);
  console.log('Chart Data', props.chartData);

  const trigger = <div style={{
    padding: '1em',
    fontWeight: 'bold',
    cursor: 'pointer',
  }}>Visualisations</div>;

  return (
    <React.StrictMode>
      <ThemeProvider theme={theme}>
        <Collapsible trigger={trigger}>
          <div className={classes.container}>
            <Cards chartData={props.chartData}/>
            <Charts chartData={props.chartData}/>
          </div>
        </Collapsible>
      </ThemeProvider>
    </React.StrictMode>
  );
};
