import React from 'react';
import {
  Card,
  Typography,
  Grid,
  CardContent,
  CardHeader,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CountUp from 'react-countup';
import AutorenewRoundedIcon from '@material-ui/icons/AutorenewRounded';
import ClearIcon from '@material-ui/icons/Clear';
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import DriveEtaIcon from '@material-ui/icons/DriveEta';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  container: {
    // display: 'flex',
    direction: 'row',
    justify: 'center',
    alignItems: 'center',
  },
  card: {
    margin: theme.spacing(5),
    borderRadius: '10px',
    borderBottom: '10px solid #e4e7e9',
  },
}));

// eslint-disable-next-line require-jsdoc
export default function Cards(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container direction="row" justify="center" alignItems="center">
        <Grid item xs={12} md={3}>
          <Card className={classes.card} elevation={20}>
            <CardContent>
              <Typography color="textSecondary" gutterBottom>
                <AutorenewRoundedIcon/> Total Complaints
              </Typography>
              <Typography variant="h5" component="h2"
                style={{ color: '#88B04B' }}>
                <CountUp start={0} end={props.chartData.total_entries} duration={1.5} separator=","/>
              </Typography>
              <Typography variant="body2" component="p">
                As of {props.chartData.latest_entry}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={3}>
          <Card className={classes.card} elevation={20}>
            <CardContent>
              <Typography color="textSecondary" gutterBottom>
                <LocalHospitalIcon/> Total Injuries
              </Typography>
              <Typography variant="h5" component="h2"
                style={{ color: '#EFC050' }}>
                <CountUp start={0} end={props.chartData.total_injuries} duration={1.5} separator=","/>
              </Typography>
              <Typography variant="body2" component="p">
                As of {props.chartData.latest_entry}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={3}>
          <Card className={classes.card} elevation={20}>
            <CardContent>
              <Typography color="textSecondary" gutterBottom>
                <ClearIcon/> Total Fatailities
              </Typography>
              <Typography variant="h5" component="h2"
                style={{ color: '#FF6F61' }}>
                <CountUp start={0} end={props.chartData.total_fatalities} duration={1.5} separator=","/>
              </Typography>
              <Typography variant="body2" component="p">
                As of {props.chartData.latest_entry}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={3}>
          <Card className={classes.card} elevation={20}>
            <CardContent>
              <Typography color="textSecondary" gutterBottom>
                <DriveEtaIcon/> Most Complained Model
              </Typography>
              <Typography variant="h5" component="h2"
                style={{ color: '#e79ba7' }}>
                {props.chartData.most_complained_model}
              </Typography>
              <Typography variant="body2" component="p">
                As of {props.chartData.latest_entry}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
}
