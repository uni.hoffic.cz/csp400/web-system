import React from 'react';
import { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Grid,
  Paper,
  Typography,
  Box,
} from '@material-ui/core';
import { Bar, Line, Pie } from 'react-chartjs-2';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  main: {
    textAlign: 'Center',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 400,
    borderColor: '#fff',
    '& label.Mui-focused': {
      color: '#FFF',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#FFF',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#FFF',
      },
      '&:hover fieldset': {
        borderColor: '#000',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#000',
      },
    },
  },

  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(3),
    borderBottom: '10px solid #5B5EA6',
  },
}));


export default function Charts(props) {
  const classes = useStyles();

  const [state, setState] = useState({
    brand: 'All',
    time: 'Alltime',
  });

  const [chartData, setChartData] = useState({});
  const [causeChartData, setCauseChartData] = useState({});
  const [mileageChartData, setMileageChartData] = useState({});
  const [transmissionChartData, setTransmissionChartData] = useState({});
  const [fuelChartData, setFuelChartData] = useState({});
  const [modelChartData, setModelChartData] = useState({});

  const chartConfig = {
    legend: { labels: { fontColor: 'white' } },
    scales: {
      yAxes: [{
        ticks: {
          fontColor: 'white',
          beginAtZero: true,
        },
      }],
      xAxes: [{
        ticks: {
          fontColor: 'white',
        },
      }],
    },
  };

  const years = props.chartData.complaints_series.map(x => x.yeartxt);

  const charts = () => {
    //get all days between two dates
    setChartData({
      labels: years,
      datasets: [
        {
          label: 'Number of Complaints',
          data: props.chartData.complaints_series.map(x => x.count),
          backgroundColor: '#CFE7AC',
          borderColor: '#88B04B',
          fill: true,
        },
        {
          label: 'Number of Injuries',
          data: props.chartData.injuries_series.map(x => x.sum),
          borderColor: '#EFC050',
          backgroundColor: '#F5D78F',
          fill: true,
        },
        {
          label: 'Number of Fatalities',
          data: props.chartData.fatalities_series.map(x => x.sum),
          borderColor: '#FF6F61',
          backgroundColor: '#FF8484',
          fill: true,
        },
      ],
    });
    setCauseChartData({
      labels: props.chartData.complaint_components.map(x => x.comp_cat),
      datasets: [
        {
          label: 'Total Amount',
          backgroundColor: '#DD4124',
          data: props.chartData.complaint_components.map(x => x.count),
        },
      ],
    });
    setMileageChartData({
      labels: props.chartData.mileages_at_complaint.map(x => x.mileage_range),
      datasets: [
        {
          label: 'Total Amount',
          backgroundColor: '#45B8AC',
          data: props.chartData.mileages_at_complaint.map(x => x.count),
        },
      ],
    });
    setTransmissionChartData({
      labels: props.chartData.transmission_types.map(x => x.trans_type),
      datasets: [
        {
          label: 'Transmission Type',
          backgroundColor: ['#1c82cc', '#59166b', '#166b17', '#6b5d16'],
          data: props.chartData.transmission_types.map(x => x.count),
        },
      ],
    });
    setFuelChartData({
      labels: props.chartData.fuel_systems.map(x => x.fuel_sys),
      datasets: [
        {
          label: 'Fuel Type',
          backgroundColor: [
            '#9B2335',
            '#1c82cc',
            '#EFC050',
            '#54791b',
            '#e38896',
            '#83b2d4',
            '#fdefd0',
            '#bbee7e',
          ],
          data: props.chartData.fuel_systems.map(x => x.count),
        },
      ],
    });
    setModelChartData({
      labels: props.chartData.most_complained_models.map(x => x.modeltxt),
      datasets: [{
        label: 'Total Amount',
        backgroundColor: '#EFC050',
        data: props.chartData.most_complained_models.map(x => x.count),
      }],
    });
  };

  useEffect(() => {
    charts();
  }, []);

  const handleChange = (event) => {
    const name = event.target.name;
    setState({ ...state, [name]: event.target.value });
    console.log(state.brand, state.time);
  };

  return (
    <div className={classes.main}>
      <Grid container direction="row" justify="center" alignItems="center"
            spacing={4}>
        <Grid item xs={5}>
          <Typography color="textSecondary" gutterBottom>
            Complaints, Injuries and Deaths
          </Typography>
          <Paper className={classes.paper} elevation="20">
            <Line data={chartData} options={chartConfig}/>
          </Paper>
        </Grid>
        <Grid item xs={5}>
          <Typography color="textSecondary" gutterBottom>
            Complaint Causes
          </Typography>
          <Paper className={classes.paper} elevation="20">
            <Bar data={causeChartData} options={chartConfig}/>
          </Paper>
        </Grid>
        <Grid item xs={5}>
          <Typography color="textSecondary" gutterBottom>
            Mileage at Complaint
          </Typography>
          <Paper className={classes.paper} elevation="20">
            <Bar data={mileageChartData} options={chartConfig}/>
          </Paper>
        </Grid>
        <Grid item xs={5}>
          <Typography color="textSecondary" gutterBottom>
            Complained Vehicle Transmission
          </Typography>
          <Paper className={classes.paper} elevation="20">
            <Pie data={transmissionChartData}
                 options={{ legend: { labels: { fontColor: 'white' } } }}/>
          </Paper>
        </Grid>
        <Grid item xs={5}>
          <Typography color="textSecondary" gutterBottom>
            Complained Vehicle Fuel Type
          </Typography>
          <Paper className={classes.paper} elevation="20">
            <Pie data={fuelChartData}
                 options={{ legend: { labels: { fontColor: 'white' } } }}/>
          </Paper>
        </Grid>
        <Grid item xs={5}>
          <Typography color="textSecondary" gutterBottom>
            Most Complained Vehicle Models
          </Typography>
          <Paper className={classes.paper} elevation="20" mb={3}>
            <Bar data={modelChartData} options={chartConfig}/>
          </Paper>
        </Grid>
      </Grid>
      <Box m={5}/>
    </div>
  );
}
