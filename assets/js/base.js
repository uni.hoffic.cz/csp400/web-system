import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { App } from './vis/App';

window.addEventListener('load', () => {
  const appRoot = document.querySelector('#vis-root');
  const initProps = {
    'route': JSON.parse(appRoot.dataset.props).route,
    'chartData': JSON.parse(appRoot.dataset.props).data,
  };
  ReactDOM.render(React.createElement(App, initProps), appRoot);
});
